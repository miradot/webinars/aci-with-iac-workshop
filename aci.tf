## VARIABLES DECLARED IN terraform.tvars
variable "ACI_URL" {}
variable "ACI_USER" {}
variable "ACI_PASS" {}

# VARIABLES
variable "filter_groups" {
  description = "Create EPGs with these names"
  default     = [
      {filter = "web", port = "http", entry: "http"},
      {filter = "db", port = "1433", entry: "sql"}
  ]
}

# ACI PROVIDER
provider "aci" {
    username    = var.ACI_USER
    password    = var.ACI_PASS
    url         = var.ACI_URL
    insecure    = true
}

# ACI TENANT
resource "aci_tenant" "cisco_tenant" {
    name        = ""
    description = "CREATED WITH TF \\\\o/"
}

# ACI VRF
resource "aci_vrf" "cisco_vrf" {
    tenant_dn   = aci_tenant.cisco_tenant.id
    name        = "demo_VRF"
}

# ACI BRIDE-DOMAIN
resource "aci_bridge_domain" "cisco_bd" {
    tenant_dn          = aci_tenant.cisco_tenant.id
    relation_fv_rs_ctx = aci_vrf.cisco_vrf.name
    name               = "demo_BD"
}

# ACI SUBNET REFERENCING BD
resource "aci_subnet" "cisco_subnet" {
    bridge_domain_dn   = aci_bridge_domain.cisco_bd.id
    ip                 = "1.1.1.1/24"
    scope              = "public"
}

# ACI APPLICATION-PROFILE
resource "aci_application_profile" "cisco_ap" {
    tenant_dn          = aci_tenant.cisco_tenant.id
    name               = "demo_AP"
}

# ACI CONTRACT
resource "aci_contract" "aci_contract" {
    count       = length(var.filter_groups)
    tenant_dn   = aci_tenant.cisco_tenant.id
    name        = lookup(var.filter_groups[count.index], "filter")
}

# ACI CONTRACT SUBJECT REFERENCING CONTRACT AND FILTER
resource "aci_contract_subject" "aci_contract_subject" {
    count         = length(var.filter_groups)
    contract_dn   = aci_contract.aci_contract[count.index].id
    name          = lookup(var.filter_groups[count.index], "filter")
    relation_vz_rs_subj_filt_att = [aci_filter.aci_filter[count.index].name]
}

# ACI FILTER
resource "aci_filter" "aci_filter" {
    count     = length(var.filter_groups)
    tenant_dn = aci_tenant.cisco_tenant.id
    name      = lookup(var.filter_groups[count.index], "filter")
}

# ACI FILTER ENTRY
resource "aci_filter_entry" "filter_entry" {
    count       = length(var.filter_groups)
    name        = lookup(var.filter_groups[count.index], "entry")
    filter_dn   = aci_filter.aci_filter[count.index].id
    ether_t     = "ip"
    prot        = "tcp"
    d_from_port = lookup(var.filter_groups[count.index], "port")
    d_to_port   = lookup(var.filter_groups[count.index], "port")
}

# ACI ENDPOINT-GROUP WEB
resource "aci_application_epg" "cisco_epg_web" {
    application_profile_dn = aci_application_profile.cisco_ap.id
    relation_fv_rs_bd      = aci_bridge_domain.cisco_bd.name
    name                   = "web"
    relation_fv_rs_prov    = ["web"]
    relation_fv_rs_cons    = ["db"]
}

# ACI ENDPOINT-GROUP DB
resource "aci_application_epg" "cisco_epg_db" {
    application_profile_dn = aci_application_profile.cisco_ap.id
    relation_fv_rs_bd      = aci_bridge_domain.cisco_bd.name
    name                   = "db"
    relation_fv_rs_prov    = ["db"]
    relation_fv_rs_cons    = []
}

module "customer_1" {
    source = "./aci_default_customer"
    tenant_name = "NEW_TENANT_DEMO_IAC1"
    vrf_name = "test"
    bd_name = "test"
    epg_name = "test"
    ap_name = "test"
    network = "2.2.2.2"
}
